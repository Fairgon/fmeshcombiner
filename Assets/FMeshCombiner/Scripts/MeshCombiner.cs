﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace FMeshCombiner
{
    public class MeshCombiner : MonoBehaviour
    {
        private List<MeshData> data = new List<MeshData>();

        private List<Transform> children = new List<Transform>();

        private List<GameObject> subMeshes = new List<GameObject>();

        public bool done = false;
        public bool found = false;
        
        private bool hasLods = false;

        public void Find()
        {
            DestroySubMeshes();
            data = new List<MeshData>();

            if (found)
            {
                found = false;

                SetChildrenState(true);
            }

            hasLods = TryFindLods();

            List<MeshFilter> meshFilters = new List<MeshFilter>(transform.GetComponentsInChildren<MeshFilter>());

            children = new List<Transform>(transform.GetComponentsInChildren<Transform>());
            children.Remove(transform);

            if (!hasLods && meshFilters.Count == 0)
            {
                Debug.LogWarning("No meshes to merge!");

                return;
            }
            
            if(hasLods)
            { 
                for (int i = 0; i < meshFilters.Count; ++i)
                {
                    if (meshFilters[i].transform.parent.GetComponent<LODGroup>() != null)
                    {
                        meshFilters.RemoveAt(i);

                        i--;
                    }
                }
            }

            if (meshFilters.Count > 0)
                data.Add(new MeshData(0, meshFilters));

            found = true;
        }

        private bool TryFindLods()
        {
            List<LODGroup> lodGroups = new List<LODGroup>(transform.GetComponentsInChildren<LODGroup>());

            if (lodGroups.Count == 0)
                return false;
            
            lodGroups.Remove(GetComponent<LODGroup>());

            for (int i = 0; i < lodGroups.Count; ++i)
            {
                LOD[] lods = lodGroups[i].GetLODs();

                for (int lodLevel = 0; lodLevel < lods.Length; ++lodLevel)
                {
                    Renderer[] renderers = lods[lodLevel].renderers;

                    if (renderers == null || renderers.Length == 0) continue;

                    List<MeshFilter> meshes = new List<MeshFilter>();

                    for (int j = 0; j < renderers.Length; ++j)
                    {
                        MeshFilter mf = renderers[j].GetComponent<MeshFilter>();

                        if (mf != null) meshes.Add(mf);
                    }

                    data.Add(new MeshData(lodLevel, meshes));
                }
            }

            return true;
        }

        public void Combine()
        {
            //if (done) DestroySubMeshes();

            if (hasLods) CombineLOD();
            else CombineSimple();

            done = true;
        }

        public void DestroySubMeshes()
        {
            foreach(GameObject subMesh in subMeshes)
            {
                if(subMesh)
                    DestroyImmediate(subMesh);
            }

            subMeshes.Clear();
        }

        private void CombineSimple()
        {
            CreateMesh(GetMeshGroup(0), 0);
            
            SetChildrenState(false);
        }
        
        private void CombineLOD()
        {
            MeshFilter meshFilter = GetComponent<MeshFilter>();
            MeshRenderer meshRenderer = GetComponent<MeshRenderer>();

            LODGroup lodGroup = GetComponent<LODGroup>();

            if (!lodGroup) lodGroup = gameObject.AddComponent<LODGroup>();
            
            int lodCount = GetLodCount();

            var lodGroupCount = lodGroup.lodCount;

            lodGroupCount = lodCount;

            List<LOD> lods = new List<LOD>();

            for (int lodLevel = 0; lodLevel < lodCount; ++lodLevel)
            {
                List<MeshFilter> meshFilters = new List<MeshFilter>(GetMeshGroup(lodLevel));
                
                List<Renderer> renderers = CreateMesh(meshFilters, lodLevel);
                
                float th = (1.0f / lodCount) * (lodCount - (lodLevel + 1));

                LOD lod = new LOD(th, renderers.ToArray());

                lods.Add(lod);
            }

            lodGroup.SetLODs(lods.ToArray());
            lodGroup.RecalculateBounds();

            SetChildrenState(false);
        }

        private List<Renderer> CreateMesh(List<MeshFilter> meshFilters, int lodLevel)
        {
            List<Renderer> subRenderers = new List<Renderer>();

            Material[] materials = GetMaterials(meshFilters).ToArray();

            Matrix4x4 myTransform = transform.worldToLocalMatrix;

            List<CombineInstance> combines = new List<CombineInstance>();
            var combine = new CombineInstance();

            int vertexCount = 0;
            int number = 0;

            Transform parent = CreateChild(transform, "Lod_" + lodLevel, lodLevel).transform;

            for (int i = 0; i < meshFilters.Count; ++i)
            {
                vertexCount += meshFilters[i].sharedMesh.vertexCount;

                if(vertexCount > 65000)
                {
                    vertexCount = 0;

                    subRenderers.Add(CreateSubMesh(combines, number, parent, materials));

                    combines.Clear();

                    number++;
                }

                combine.mesh = meshFilters[i].sharedMesh;
                combine.transform = myTransform * meshFilters[i].transform.localToWorldMatrix;

                combines.Add(combine);
            }

            subRenderers.Add(CreateSubMesh(combines, number, parent, materials));

            return subRenderers;
        }
        
        private Renderer CreateSubMesh(List<CombineInstance> combines, int number, Transform parent, Material[] materials)
        {
            Mesh mesh = new Mesh();
            
            mesh.CombineMeshes(combines.ToArray());

            GameObject subMesh = CreateChild(parent, "SubMesh_" + number, number);

            subMesh.AddComponent<MeshFilter>().mesh = mesh;
            subMesh.AddComponent<MeshRenderer>().materials = materials;
           
            return subMesh.GetComponent<MeshRenderer>();
        }

        private GameObject CreateChild(Transform parent, string name, int siblingIndex)
        {
            Transform child = new GameObject().transform;

            child.SetParent(parent);
            child.localPosition = Vector3.zero;
            child.SetSiblingIndex(siblingIndex);

            child.name = name;

            subMeshes.Add(child.gameObject);

            return child.gameObject;
        }
        
        public void SetChildrenState(bool state)
        {
            for (int i = 0; i < children.Count; ++i)
            {
                children[i].gameObject.SetActive(state);
            }
        }

        private int GetLodCount()
        {
            int count = 0;

            for (int i = 0; i < data.Count; ++i) count = Mathf.Max(data[i].LodLevel, count);

            return (count + 1);
        }

        private List<MeshFilter> GetMeshGroup(int lodLevel)
        {
            List<MeshFilter> list = new List<MeshFilter>();

            for (int i = 0; i < data.Count; ++i)
            {
                if (data[i].LodLevel == lodLevel)
                {
                    for (int j = 0; j < data[i].MeshFilters.Count; ++j)
                    {
                        list.Add(data[i].MeshFilters[j]);
                    }
                }
            }

            return list;
        }
        
        private List<Material> GetMaterials(List<MeshFilter> meshFilters)
        {
            List<Material> materials = new List<Material>();

            for (int i = 0; i < meshFilters.Count; ++i)
            {
                var list = meshFilters[i].GetComponent<MeshRenderer>().sharedMaterials;

                for (int j = 0; j < list.Length; ++j)
                {
                    if (!materials.Contains(list[j])) materials.Add(list[j]);
                }
            }

            return materials;
        }
    }
}


