﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace FMeshCombiner
{
    [CustomEditor(typeof(MeshCombiner))]
    public class MCInspector : Editor
    {
        private MeshCombiner meshCombiner;

        private void OnEnable()
        {
            meshCombiner = (MeshCombiner)target;
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.LabelField("Simple mesh combiner", new GUIStyle() { alignment = TextAnchor.MiddleCenter, fontSize = 16 });
            
            EditorGUILayout.Space();

            if (GUILayout.Button("Find Meshes"))
            {
                meshCombiner.Find();
            }
            
            if (meshCombiner.found)
            {
                EditorGUILayout.Space();
                
                if (GUILayout.Button("Combine"))
                {
                    meshCombiner.Combine();
                }

                if(meshCombiner.done)
                {
                    if (GUILayout.Button("Cancel (Destroy SubMeshes)"))
                    {
                        meshCombiner.DestroySubMeshes();
                        meshCombiner.SetChildrenState(true);

                        meshCombiner.done = false;
                    }
                }
            }
        }
    }
}
