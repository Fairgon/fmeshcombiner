﻿using UnityEngine;
using System.Collections.Generic;

namespace FMeshCombiner
{
    public class MeshData
    {
        public int LodLevel = 0;

        public List<MeshFilter> MeshFilters;

        public MeshData(int lodLevel, List<MeshFilter> meshFilters)
        {
            LodLevel = lodLevel;

            MeshFilters = new List<MeshFilter>(meshFilters);
        }
    }
}
